CC=gcc
CFLAGS=-Wall
LIBS=-lm

all: Zadanie1 Zadanie2

Zadanie1: Zadanie1.o mean.o median.o stdev.o
	$(CC) $(CFLAGS) -o Zadanie1 Zadanie1.o mean.o median.o stdev.o $(LIBS)
Zadanie1.o: Zadanie1.c fun.h
	$(CC) $(CFLAGS) -c Zadanie1.c
Zadanie2: Zadanie2.o mean.o median.o stdev.o
	$(CC) $(CFLAGS) -o Zadanie2 Zadanie2.o mean.o median.o stdev.o $(LIBS)
Zadanie2.o: Zadanie2.c fun.h
	$(CC) $(CFLAGS) -c Zadanie2.c
mean.o: mean.c
	$(CC) $(CFLAGS) -c mean.c
median.o: median.c
	$(CC) $(CFLAGS) -c median.c
stdev.o: stdev.c fun.h
	$(CC) $(CFLAGS) -c stdev.c 