#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fun.h"


int main()
{
	printf("Wczytywanie pliku i wyliczanie statystyk - tablice dynamiczne.\n\n");
	
	float *x;
	x = (float *)malloc(50*sizeof(float));
	
	float *y;
	y = (float *)malloc(50*sizeof(float));
	
	float *rho;
	rho = (float *)malloc(50*sizeof(float));
	
	FILE *p;
	char c[15] = "";
	int i = 0;
	char *a;
	float b = 0.0;
	
	p = fopen("P0001_attr.rec", "r+");
	fgets(c, 15, p);		// przesuwa, aby pomin�� linijk� z tytu�ami kolumn
	fscanf(p, "%s", &c);
	while(feof(p)==0)
	{
		b=strtod(c, &a);		// dla lp
		
		fscanf(p, "%s", &c);		//dla x
		b=strtod(c, &a);
		x[i]=b;
		
		fscanf(p, "%s", &c);		//dla y
		b=strtod(c, &a);
		y[i]=b;
		
		fscanf(p, "%s", &c);		//dla rho
		b=strtod(c, &a);
		rho[i]=b;
		
		fscanf(p, "%s", &c);
		i++;
	}
	
	float mediana = 0.0;
	mediana = median(x, 50);
	
	float srednia = 0.0;
	srednia = mean(x, 50);
	
	float odchyl = 0.0;
	odchyl = stdev(x, 50, srednia);
	
	fprintf(p, "Statystyka\t mediana\t srednia\t odchylenie \n");
	fprintf(p, "X \t %f \t %f \t %f \n", mediana, srednia, odchyl);
	
	mediana = median(y, 50);
	srednia = mean(y,50);
	odchyl = stdev(y, 50, srednia);
	fprintf(p, "Y \t %f \t %f \t %f \n", mediana, srednia, odchyl);
	
	mediana = median(rho, 50);
	srednia = mean(rho, 50);
	odchyl = stdev(rho, 50, srednia);
	fprintf(p, "RHO \t %f \t %f \t %f \n", mediana, srednia, odchyl);
	
	fclose(p);
	free(x);
	free(y);
	free(rho);
	
	system ("PAUSE");
	
	return 0;
}

