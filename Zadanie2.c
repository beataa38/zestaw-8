#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include "fun.h"


int main()
{
    printf("Wczytywanie pliku i wyliczanie statystyk - struktury.\n\n");

    int i = 0;
    float X[50];
    float Y[50];
    float RHO[50];
    char str[11];

    struct Plik_attr
    {
        char LP[10];
        float X;
        float Y;
        float RHO;

    };
    struct Plik_attr plik_[50];

    FILE *plik;
    plik = fopen("P0001_attr.rec", "r+");

    if (plik == NULL)
    {
        perror("Blad otwarcia pliku: ");
        exit(-10);
    }
    else
    {
        fseek(plik, 12, SEEK_SET);
        while (fscanf(plik, "%s    %f  %f  %f", plik_[i].LP, &plik_[i].X, &plik_[i].Y, &plik_[i].RHO) != EOF)
        {
            i++;
        }
    }
	
	/*
    for (i = 0; i < 50; i++)
    {
        printf("LP:  %s\n", plik_[i].LP);
        printf("X:  %.5f\n", plik_[i].X);
        printf("Y:  %.5f\n", plik_[i].Y);
        printf("RHO:  %.3f\n", plik_[i].RHO);
        printf("\n");
    }
	*/
	
    for (i = 0; i < 50; i++)
    {
        X[i] = plik_[i].X;
        Y[i] = plik_[i].Y;
        RHO[i] = plik_[i].RHO;
    }

    struct Plik_attr mean_all = {"Srednia", mean(X, 50), mean(Y, 50), mean(RHO, 50)};
    struct Plik_attr median_all = {"Mediana", median(X, 50), median(Y, 50), median(RHO, 50)};
    struct Plik_attr stdev_all = {"Odchyl.", stdev(X, 50, mean_all.X), stdev(Y, 50, mean_all.Y), stdev(RHO, 50, mean_all.RHO)};
	
	/*
    printf("Statystyka\tX\t\tY\t\tRHO\n");
    printf("%s:\t\t%.5f\t\t%.5f\t\t%.3f\n", mean_all.LP, mean_all.X, mean_all.Y, mean_all.RHO);
    printf("%s:\t\t%.5f\t\t%.5f\t\t%.3f\n", median_all.LP, median_all.X, median_all.Y, median_all.RHO);
    printf("%s:\t\t%.5f\t\t%.5f\t\t%.3f\n", stdev_all.LP, stdev_all.X, stdev_all.Y, stdev_all.RHO);
	*/
	
    fseek(plik, 1404, SEEK_SET);

    if (fgets(str, 11, plik) != "Statystyka")
    {
        fprintf(plik, "Statystyka\t\tX\t\tY\t\tRHO\n");
        fprintf(plik, "%s:\t\t%.5f\t\t%.5f\t\t%.3f\n", mean_all.LP, mean_all.X, mean_all.Y, mean_all.RHO);
        fprintf(plik, "%s:\t\t%.5f\t\t%.5f\t\t%.3f\n", median_all.LP, median_all.X, median_all.Y, median_all.RHO);
        fprintf(plik, "%s:\t\t%.5f\t\t%.5f\t\t%.3f\n", stdev_all.LP, stdev_all.X, stdev_all.Y, stdev_all.RHO);
    }

    fclose(plik);

    system ("PAUSE");

    return 0;
}