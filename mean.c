#include "fun.h"

float mean(float *arr, int len)
{
    int i = 0;
    float sum = 0.0;
    float mean = 0.0;

    for (i = 0; i < len; i++)
    {
        sum += arr[i];
    }
    mean = sum / len;

    return mean;
}

