#include "fun.h"

float median(float *arr, int len)
{
    int i = 0;
    int j = 0;
    float k = 0.0;

    for (i = 1; i < len; i++)       //sortujemy, aby otrzyma� ci�g niemalej�cy
    {
        k = arr[i];
        j = i - 1;
        while (j >= 0 && arr[j] > k)
        {
            arr[j + 1] = arr[j];
            j --;
        }
        arr[j + 1] = k;
    }

    float median = 0.0;

    if (len % 2 == 0)
    {
        median = (arr[(len/2) - 1] + arr[len/2]) / 2.0;
    }
    else
    {
        median = arr[len/2];
    }

    return median;
}

