#include "fun.h"
#include <math.h>

float stdev(float *arr, int len, float mean)
{
    float sum = 0.0;
    float stdev = 0.0;
    float k = 0.0;
    int i = 0;

    for (i = 0; i < len; i++)
    {
        k = arr[i] - mean;
        k = pow(k, 2);
        sum += k;
    }

    stdev = sqrt(sum / len);

    return stdev;
}
